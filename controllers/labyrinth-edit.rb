#!/usr/bin/env ruby
require '../core.rb'

begin
  uid = Core.auth

  room_id = Core.input('room_id') || 1
  room = Labyrinth.get_room room_id

  Core.render('Labyrinth/edit', {
    'uid' => uid,
    'owner' => room['owner'],
    'contents' => room['contents'],
    'name' => room['name'],
  })
rescue Exception => e
  Core.log "Exception occurred in #{self.class.name}.#{__method__}"
  Core.log e
end
