#!/usr/bin/env ruby
require '../core.rb'

begin
  uid = Core.auth

  if uid then
    action = Core.input 'action'
    tag_id = Core.input 'tag_id'
    project_id = Core.input 'project_id'
    name = Core.input 'name'
    value = Core.input 'value'

    if action == 'add-tag' then
      Tags.add uid, 'Unnamed Tag'
    elsif action == 'add-project' then
      Projects.add uid, 'Unnamed Project'
    elsif action == 'archive-tag' then
      if tag_id then
        Tags.archive uid, tag_id
      else
        # TODO: return error
      end
    elsif action == 'archive-project' then
      if project_id then
        Projects.archive uid, project_id
      else
        # TODO: return error
      end
    elsif action == 'name-tag' then
      if tag_id and name then
        Tags.update uid, tag_id, name
      else
        # TODO: return error
      end
    elsif action == 'name-project' then
      if project_id and name then
        Projects.update uid, project_id, name
      else
        # TODO: return error
      end
    elsif action == 'group-tags' then
      # TODO: implement
    elsif action == 'group-projects' then
      # TODO: implement
    elsif action == 'carve-inscription' then
      if name and value then
        Inscriptions.carve uid, name, value
      else
        # TODO: return error
      end
    end
  end

  Core.render('blank', {})
rescue Exception => e
  Core.log "Exception occurred in #{self.class.name}.#{__method__}"
  Core.log e
end
