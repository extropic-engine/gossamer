#!/usr/bin/env ruby
require '../core.rb'
require 'yaml'

begin
  notification = ''
  notification_type = ''

  requested_uid = Core.input 'uid'
  view_uid = nil
  uid = Core.auth

  events = {}
  if requested_uid and requested_uid.to_i > 0 then
    public_pages = Inscriptions.read(requested_uid, 'public_pages')
    if (public_pages.is_a? Array and public_pages.has_key? 'exegesis') or public_pages == 'exegesis' then
      view_uid = requested_uid.to_i
    else
      notification = 'That person has chosen to make their exegesis a personal affair.'
      notification_type = 'alert-error'
      view_uid = uid if uid
    end
  else
    view_uid = uid if uid
  end

  if view_uid then
    events = Events.generate_exegesis view_uid
    # An exposition is an element displaying a number of hours or a percentage for a specific tag
    # or project.
    exp = Inscriptions.read(view_uid, 'expositions')
    exposition_rows = YAML::load(exp) if exp
    # TODO: load exposition rows with data from events['hours'] and events['percents']
  end

  Core.render('exegesis', {
    'uid' => uid,
    'notification' => notification,
    'notification_type' => notification_type,
    'years' => events['years'],
    'hours' => events['hours'],
    'percents' => events['percents'],
    'exposition_rows' => exposition_rows
  })
rescue Exception => e
  Core.log "Exception occurred in #{self.class.name}.#{__method__}"
  Core.log e
end
