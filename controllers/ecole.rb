#!/usr/bin/env ruby
require '../core.rb'

begin
  uid = Core.auth

  Core.render('ecole', {
    'uid' => uid
  })
rescue Exception => e
  Core.log "Exception occurred in #{self.class.name}.#{__method__}"
  Core.log e
end
