#!/usr/bin/env ruby
require '../core.rb'
require 'yaml'

begin
  notification = ''
  notification_type = ''

  username = Core.input 'username'
  password = Core.input 'password'

  uid = Core.auth(username, password)

  if uid then
    projects = Projects.get_active uid
    tags = Tags.get_active uid
    archived_projects = Projects.get_archived uid
    archived_tags = Tags.get_archived uid
    inscriptions = Inscriptions.read_all uid
    inscriptions['expositions'] = YAML::load(inscriptions['expositions']) if inscriptions['expositions']
  else
    if Core.post? then
      notification = 'Invalid username or password.'
      notification_type = 'alert-error'
    end
  end

  Core.render('panopticon', {
    'uid' => uid,
    'notification' => notification,
    'notification_type' => notification_type,
    'projects' => projects,
    'tags' => tags,
    'archived_projects' => archived_projects,
    'archived_tags' => archived_tags,
    'inscriptions' => inscriptions
  })
rescue Exception => e
  Core.log "Exception occurred in #{self.class.name}.#{__method__}"
  Core.log e
end
