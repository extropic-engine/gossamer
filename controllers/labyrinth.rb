#!/usr/bin/env ruby
require '../core.rb'

begin
  uid = Core.auth

  room_url = Core.input('r') || 1

  room_list = Quire.get_labyrinth_rooms

  contents = ''
  contents << File.open("#{Core::BASEDIR}/views/Labyrinth/header.html", 'rb').read
  contents << 'coming soon'
  contents << File.open("#{Core::BASEDIR}/views/Labyrinth/footer.html", 'rb').read
  Core.render_dynamic(contents, {
    'uid' => uid
  })
rescue Exception => e
  Core.log "Exception occurred in #{self.class.name}.#{__method__}"
  Core.log e
end
