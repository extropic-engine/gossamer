#!/usr/bin/env ruby
require '../core.rb'

begin
  uid = Core.auth

  mantra = Mantras.get_random_mantra
  Core.render('mantra', {
    'uid' => uid,
    'mantra' => mantra
  })
rescue Exception => e
  Core.log "Exception occurred in #{self.class.name}.#{__method__}"
  Core.log e
end
