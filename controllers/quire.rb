#!/usr/bin/env ruby
require '../core.rb'

begin
  notification = ''
  notification_type = ''

  username = Core.input 'username'
  password = Core.input 'password'

  uid = Core.auth(username, password)
  leaf_id = Core.input 'leaf'

  if uid then
    registrum = Quire.get_registrum uid
    if leaf_id then
      leaf = Quire.read_leaf leaf_id
      if uid != leaf['user_id'] then
        leaf = nil
      end
    end
  else
    if Core.post? then
      notification_type = 'alert-error'
      notification = 'Invalid username or password.'
    end
  end

  Core.render('quire', {
    'uid' => uid,
    'notification_type' => notification_type,
    'notification' => notification,
    'registrum' => registrum,
    'leaf' => leaf
  })
rescue Exception => e
  Core.log "Exception occurred in #{self.class.name}.#{__method__}"
  Core.log e
end
