#!/usr/bin/env ruby
require '../core.rb'

begin
  notification = ''
  notification_type = ''

  start = Core.input 'start'
  username = Core.input 'username'
  password = Core.input 'password'
  project = Core.input 'project'
  hours = Core.input 'hours'
  date = Core.input 'date'
  tags_in = Core.input 'tags'

  uid = Core.auth(username, password)

  if uid then
    if Core.post? then
      notification_type = 'alert-error'
      if !project then
        notification = 'Please enter a project name.'
      elsif !hours then
        notification = 'Please enter a number of hours.'
      else
        if date and Core.validate_date_format(date)
            notification = 'Date must be in the format YYYY-MM-DD.'
        else
          date = 'now' unless date
          success = Events.log(uid, project, date, Integer(hours.to_f * 3600), tags_in || [])
          if success
            notification_type = 'alert-success'
            notification = 'Log recorded.'
          else
            # TODO: better failure messages that pinpoint the cause of the error
            notification = 'Log could not be recorded. Check your input data.'
          end
        end
      end
    end
    events = Events.get_event_rows(uid, start ? start : 0, 50, true);
    projects = Projects.get_active uid
    tags = Tags.get_active uid
  else
    if Core.post? then
      notification = 'Invalid username or password.'
      notification_type = 'alert-error'
    end
  end

  Core.render('ledger', {
    'uid' => uid,
    'notification' => notification,
    'notification_type' => notification_type,
    'events' => events,
    'projects' => projects,
    'tags' => tags
  })
rescue Exception => e
  Core.log "Exception occurred in #{self.class.name}.#{__method__}"
  Core.log e
end