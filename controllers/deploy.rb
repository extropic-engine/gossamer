#!/usr/bin/env ruby
require '../core.rb'

begin
  result = `git pull`
  Core.render('deploy', {'result' => result})
rescue Exception => e
  Core.log "Exception occurred in #{self.class.name}.#{__method__}"
  Core.log e
end
