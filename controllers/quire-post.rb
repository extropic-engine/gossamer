#!/usr/bin/env ruby
require '../core.rb'

begin
  uid = Core.auth

  if uid then
    action = Core.input 'action'
    leaf_id = Core.input 'leaf_id'

    if action == 'fetch-leaf' then
      leaf = Quire.read_leaf leaf_id
      if leaf['public'] == 1 or leaf['user_id'] == uid then
        # TODO: return the contents of the leaf
      else
        # TODO: return an error
      end
    elsif action == 'save-leaf' then
      # TODO: save the leaf
    end
  end

  Core.render('blank', {})
rescue Exception => e
  Core.log "Exception occurred in #{self.class.name}.#{__method__}"
  Core.log e
end
