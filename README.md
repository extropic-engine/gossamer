[![Stories in Ready](https://badge.waffle.io/extropic-engine/Gossamer.png?label=ready)](http://waffle.io/extropic-engine/Gossamer)

Abstract
========

Inspired by [XXIIVV](http://wiki.xxiivv.com/Horaire).

Structure
=========

Ruby CGI is used to run the scripts. No persistent server process is needed
apart from Apache itself. mod\_ruby is not needed. The website was designed to
run on nearlyfreespeech.net's servers and so is structured with that
architecture in mind.

The files are organized in a basic Model-View-Controller layout. Liquid is
used as the HTML templating engine, with SQLite providing the model backend.

Deployment
==========

Make sure you have the necessary gems installed.

    gem install liquid sqlite3-ruby htmlentities

Initialize the database by running `homunculi/init.rb`
Then, add an admin user by running `homunculi/add_user.rb`

Make sure your database folder, database file, and log files are writeable by the server:

    touch Gossamer.log
    chmod a+w db db/Gossamer.db Gossamer.log

To configure your Apache like NFSN's, find the line in your httpd.conf that starts with

    AddHandler cgi-script

and add `.rb` to the list. of accepted file types.

Then run `apachectl -k graceful`.