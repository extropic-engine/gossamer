#!/usr/bin/env ruby
require '../core.rb'

begin
  db = SQLite3::Database.new "#{Core::DBPATH}"
  #
  # Create tables
  #
  db.execute "CREATE TABLE IF NOT EXISTS users(
    user_id INTEGER PRIMARY KEY,
    username STRING,
    password STRING,
    salt STRING,
    UNIQUE(username))"

  db.execute "CREATE TABLE IF NOT EXISTS inscriptions(
    inscription_id INTEGER PRIMARY KEY,
    user_id INTEGER REFERENCES users(user_id),
    name STRING,
    value STRING,
    UNIQUE(user_id, name))"

  db.execute "CREATE TABLE IF NOT EXISTS quire_leaves(
    quire_id INTEGER PRIMARY KEY,
    url STRING,
    title STRING,
    content STRING,
    created_on INTEGER,
    updated_on INTEGER,
    type INTEGER,
    user_id INTEGER REFERENCES users(user_id),
    UNIQUE(url))"

  db.execute "CREATE TABLE IF NOT EXISTS projects(
    project_id INTEGER PRIMARY KEY,
    name STRING,
    user_id INTEGER REFERENCES users(user_id),
    archived INTEGER)"

  db.execute "CREATE TABLE IF NOT EXISTS events(
    event_id INTEGER PRIMARY KEY,
    project_id INTEGER REFERENCES projects(project_id),
    start INTEGER,
    duration INTEGER,
    user_id INTEGER REFERENCES users(user_id))"

  db.execute "CREATE TABLE IF NOT EXISTS tags(
    tag_id INTEGER PRIMARY KEY,
    name STRING,
    user_id INTEGER REFERENCES users(user_id),
    archived INTEGER)"

  db.execute "CREATE TABLE IF NOT EXISTS tag_groups(
    tag_group_id INTEGER PRIMARY KEY,
    name STRING
    user_id INTEGER REFERENCES users(user_id))"

  db.execute "CREATE TABLE IF NOT EXISTS tag_group_tags(
    tag_group_id INTEGER REFERENCES tag_groups(tag_group_id),
    tag_id INTEGER REFERENCES tags(tag_id),
    ordering INTEGER,
    UNIQUE(tag_group_id, tag_id))"

  db.execute "CREATE TABLE IF NOT EXISTS event_tags(
    event_id INTEGER REFERENCES events(event_id),
    tag_id INTEGER REFERENCES tags(tag_id),
    UNIQUE(event_id, tag_id))"

  db.execute "CREATE TABLE IF NOT EXISTS mantras(
    mantra_id INTEGER PRIMARY KEY,
    content STRING,
    source STRING,
    archived INTEGER,
    user_id INTEGER REFERENCES users(user_id))"
rescue SQLite3::Exception => e
  Core.log "Exception occurred."
  Core.log e
ensure
  db.close if db
end
