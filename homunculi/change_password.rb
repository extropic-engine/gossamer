#!/usr/bin/env ruby
require '../core.rb'

begin
  puts 'Username:'
  username = gets.strip
  user = User.get(username)
  if !user then
    puts 'User not found.'
    return
  end
  puts 'New password:'
  password = gets.strip
  salt = Time.now.to_i
  p_hash = Digest::SHA256.hexdigest "#{password}#{salt}"
  user.password = p_hash
  user.salt = salt
  user.save

  if user.saved? then
    puts 'Updated password.'
  else
    puts 'Could not update password.'
  end
end
