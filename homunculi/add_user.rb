#!/usr/bin/env ruby
require '../core.rb'

begin
  puts 'Username:'
  username = gets.strip
  puts 'Password:'
  password = gets.strip
  user = User.get(username)
  if user then
    puts "User was already in the database."
    return
  end

  salt = Time.now.to_i
  p_hash = Digest::SHA256.hexdigest "#{password}#{salt}"
  user = User.create(:username => username, :password => p_hash, :salt => salt)
  if user.saved? then
    puts "Added user to database."
  else
    puts "User could not be added to database."
  end
end
