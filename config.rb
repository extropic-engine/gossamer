PROJECT_NAME = 'Gossamer'
BASEDIR = File.expand_path(File.dirname(__FILE__))
DBPATH = "#{BASEDIR}/db/#{PROJECT_NAME}.db"
VERSION = '&#x03b3;1'


if ENV['NFSN_SITE_NAME'] == 'extropicstudios' then
  # hardcoded for now
  SITE_URL = 'https://www.extropicstudios.com'
  DB_USER = 'gossamer'
  DB_PASS = '{secret}'
  DB_HOST = 'extropicstudios.db'
  DB_NAME = 'gossamer'
else
  SITE_URL = 'http://localhost'
  DB_USER = 'gossamer'
  DB_PASS = 'password'
  DB_HOST = 'localhost'
  DB_NAME = 'gossamer'
end

BASE_URL = "#{SITE_URL}/#{PROJECT_NAME.downcase}"
