$(document).ready(function() {

  var endpoint = 'quire-post.rb';

  $('.quire-leaf').click(function() {
    $.post(endpoint, { action: 'fetch-leaf', leaf_id: $(this).data('leaf-id')}, function(data) {
      // TODO: get success result and render it
      // TODO: show error on failure
    });
  });

  $('#save-leaf').click(function() {
    $.post(endpoint, {
      action: 'save-leaf',
      title: $('#leaf-title').val(),
      contents: $('#leaf-contents').val()
    }, function(data) {
      // TODO: take action on success or failure
    });
  });

});