$(document).ready(function() {

  var endpoint = 'panopticon-post.rb';

  // Projects and tags

  $('a.archive-project').click(function() {
    $.post(endpoint, { action: 'archive-project', project_id: $(this).data('project-id')}, function() {
      // TODO show error on failure
      $(this).parent().fadeOut();
    });
  });

  $('a.archive-tag').click(function() {
    $.post(endpoint, { action: 'archive-tag', tag_id: $(this).data('tag-id')}, function() {
      // TODO show error on failure
      $(this).parent().fadeOut();
    });
  });

  $('input.name-project').blur(function() {
    $.post(endpoint, {
      action: 'name-project',
      project_id: $(this).data('project-id'),
      name: $(this).val()
    }, function() {
      // TODO: figure out how to indicate that the name was saved
    });
  });

  $('input.name-tag').blur(function() {
    $.post(endpoint, {
      action: 'name-tag',
      tag_id: $(this).data('tag-id'),
      name: $(this).val()
    }, function() {
      // TODO: figure out how to indicate that the name was saved
    });
  });

  $('a.add-project').click(function() {
    $.post(endpoint, { action: 'add-project' }, function() {
      // TODO: figure out how to display adding a project
    });
  });

  $('a.add-tag').click(function() {
    $.post(endpoint, { action: 'add-tag' }, function() {
      // TODO: figure out how to display adding a tag
    });
  });

  // Simple inscriptions

  $('.btn-group.inscription > .btn').click(function() {
    $.post(endpoint, {
      action: 'carve-inscription',
      name: $(this).parent().data('name'),
      value: $(this).text().trim()
    });
  });

  // Expositions
  $('.exposition-add-new').click(function() {
    $('.exposition-add-new').before('<p>test</p>');
    // TODO: complete this function
  });

  $('.exposition-add-row').click(function() {
    $('.exposition-add-row').before('<p>test</p>');
    // TODO: complete this function
  });
});