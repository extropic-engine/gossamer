class Room
  include DataMapper::Resource

  property   :id,         Serial
  property   :url,        String,   :required => true, :unique => true
  property   :title,      Text
  property   :content,    Text
  property   :created_on, DateTime
  property   :updated_on, DateTime
  belongs_to :user,                 :required => true
end