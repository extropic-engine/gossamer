require '../core.rb'

class Tags

  # Add a new tag.
  def self.add(user_id, name)
    db = SQLite3::Database.open "#{Core::DBPATH}"
    name = Core.encode name
    db.execute "INSERT INTO tags (user_id, name, archived) VALUES (?, ?, 0)", user_id, name
    # TODO: return the ID of the newly inserted tag
  rescue SQLite3::Exception => e
    Core.log "Exception occurred in #{self.class.name}.#{__method__}"
    Core.log e
  ensure
    db.close if db
  end

  # Get list of all active tags for a user
  def self.get_active(user_id)
    db = SQLite3::Database.open "#{Core::DBPATH}"
    results = []
    db.execute "SELECT tag_id, name FROM tags WHERE user_id = ? AND archived = 0", user_id do |row|
      results << {'id' => row[0], 'name' => row[1]}
    end
    return results
  rescue SQLite3::Exception => e
    Core.log "Exception occurred in #{self.class.name}.#{__method__}"
    Core.log e
  ensure
    db.close if db
  end

  # Get list of all archived tags for a user
  def self.get_archived(user_id)
    db = SQLite3::Database.open "#{Core::DBPATH}"
    results = []
    db.execute "SELECT tag_id, name FROM tags WHERE user_id = ? AND archived = 1", user_id do |row|
      results << {'id' => row[0], 'name' => row[1]}
    end
    return results
  rescue SQLite3::Exception => e
    Core.log "Exception occurred in #{self.class.name}.#{__method__}"
    Core.log e
  ensure
    db.close if db
  end

  # Update the name of an existing tag
  def self.update(user_id, tag_id, name)
    db = SQLite3::Database.open "#{Core::DBPATH}"
    name = Core.encode name
    db.execute "UPDATE tags SET name = ? WHERE user_id = ? AND tag_id = ?", name, user_id, tag_id
  rescue SQLite3::Exception => e
    Core.log "Exception occurred in #{self.class.name}.#{__method__}"
    Core.log e
  ensure
    db.close if db
  end

  # Mark a tag as archived
  def self.archive(user_id, tag_id)
    db = SQLite3::Database.open "#{Core::DBPATH}"
    db.execute "UPDATE tags SET archived = 1 WHERE user_id = ? AND tag_id = ?", user_id, tag_id
  rescue SQLite3::Exception => e
    Core.log "Exception occurred in #{self.class.name}.#{__method__}"
    Core.log e
  ensure
    db.close if db
  end

  # Unmark a tag as archived
  def self.unarchive(user_id, tag_id)
    db = SQLite3::Database.open "#{Core::DBPATH}"
    db.execute "UPDATE tags SET archived = 0 WHERE user_id = ? AND tag_id = ?", user_id, tag_id
  rescue SQLite3::Exception => e
    Core.log "Exception occurred in #{self.class.name}.#{__method__}"
    Core.log e
  ensure
    db.close if db
  end

end
