require '../core.rb'

# In Sanskrit, the root word 'man-' means 'to think' and the suffix '-tra'
# means 'tool' or 'instrument'. Thus, a literal translation of 'mantra' is
# 'instrument of thought.'
class Mantras

  def self.get_random_mantra
    db = SQLite3::Database.open "#{Core::DBPATH}"
    return db.get_first_row("SELECT content FROM mantras WHERE archived = 0 ORDER BY RANDOM() LIMIT 1").first
  rescue SQLite3::Exception => e
    Core.log "Exception occurred in #{self.class.name}.#{__method__}"
    Core.log e
  ensure
    db.close if db
  end

end
