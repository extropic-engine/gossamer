require '../core.rb'

class Labyrinth

  def self.get_room(room_id)
    db = SQLite3::Database.open "#{Core::DBPATH}"
    row = db.get_first_row "SELECT user_id, name, contents FROM labyrinth WHERE room_id = ?", room_id
    return {'owner' => row[0], 'name' => row[1], 'contents' => row[2]}
  rescue SQLite3::Exception => e
    Core.log "Exception occurred in #{self.class.name}.#{__method__}"
    Core.log e
  ensure
    db.close if db
  end

end
