require '../core.rb'

# A quire, also called a gathering, was a booklet composed of four folded
# sheets of parchment or vellum. Multiple quires would then be collected
# together to form a codex.
class Quire

  PUBLIC = 1
  PRIVATE = 2
  LABYRINTH = 3

  # Gets a list of all quires with a short summary.
  # The registrum was a page at the back of a codex that would list all the
  # quires along with their signatures, in case they fell out of order or
  # were lost.
  def self.get_registrum(user_id)
    db = SQLite3::Database.open "#{Core::DBPATH}"
    results = []
    db.execute "SELECT quire_id, title, url FROM quire_leaves WHERE user_id = ?", user_id do |row|
      results << {'id' => row[0], 'title' => row[1], 'url' => row[2]}
    end
    return results
  rescue SQLite3::Exception => e
    Core.log "Exception occurred in #{self.class.name}.#{__method__}"
    Core.log e
  ensure
    db.close if db
  end

  # Save a writing into the quire.
  # Each page of a quire was called a leaf. Sheets of parchment were folded
  # to create multiple leaves. A sheet folded in half was called a folio and
  # had two leaves.
  def self.scribe_leaf(user_id, url, title, content)
    db = SQLite3::Database.open "#{Core::DBPATH}"
    url = Core.encode url
    title = Core.encode title
    content = Core.encode content
    db.execute "INSERT INTO quire_leaves (user_id, url, title, content, created_on, updated_on) VALUES (?, ?, ?, ?, strftime('%s', 'now'), strftime('%s', 'now'))", user_id, url, title, content
    id = event_id = db.get_first_row("SELECT last_insert_rowid()").first
    return id ? true : false;
  rescue SQLite3::Exception => e
    Core.log "Exception occurred in #{self.class.name}.#{__method__}"
    Core.log e
  ensure
    db.close if db
  end

  def self.read_leaf(leaf_id)
    db = SQLite3::Database.open "#{Core::DBPATH}"
    row = db.get_first_row "SELECT user_id, title, content FROM quire_leaves WHERE quire_id = ?", leaf_id
    return nil if row.nil?
    return {'user_id' => row[0], 'title' => row[1], 'content' => row[2]}
  rescue SQLite3::Exception => e
    Core.log "Exception occurred in #{self.class.name}.#{__method__}"
    Core.log e
  ensure
    db.close if db
  end

  def self.get_labyrinth_rooms
    db = SQLite3::Database.open "#{Core::DBPATH}"
    rooms = []
    db.execute "SELECT quire_id, url, title, user_id FROM quire_leaves WHERE type = #{LABYRINTH}" do |row|
      rooms << {'id' => row[0], 'url' => row[1], 'name' => row[2], 'user_id' => row[3]}
    end
    return rooms
  rescue SQLite3::Exception => e
    Core.log "Exception occurred in #{self.class.name}.#{__method__}"
    Core.log e
  ensure
    db.close if db
  end

end
