class Event
  include DataMapper::Resource

  property :id, Serial
  property :start, DateTime
  property :duration, Integer
  belongs_to :user
  #belongs_to :project
  # has and belongs to many tags
end