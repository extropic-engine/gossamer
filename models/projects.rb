require '../core.rb'

class Projects

  # Add a new project.
  def self.add(user_id, name)
    db = SQLite3::Database.open "#{Core::DBPATH}"
    name = Core.encode name
    db.execute "INSERT INTO projects (user_id, name, archived) VALUES (?, ?, 0)", user_id, name
    # TODO: return the ID of the newly inserted tag
  rescue SQLite3::Exception => e
    Core.log "Exception occurred in #{self.class.name}.#{__method__}"
    Core.log e
  ensure
    db.close if db
  end

  # Get list of all active projects for a user
  def self.get_active(user_id)
    db = SQLite3::Database.open "#{Core::DBPATH}"
    results = []
    db.execute "SELECT project_id, name FROM projects WHERE user_id = ? AND archived = 0", user_id do |row|
      results << {'id' => row[0], 'name' => row[1]}
    end
    return results
  rescue SQLite3::Exception => e
    Core.log "Exception occurred in #{self.class.name}.#{__method__}"
    Core.log e
  ensure
    db.close if db
  end

  # Get list of all archived projects for a user
  def self.get_archived(user_id)
    db = SQLite3::Database.open "#{Core::DBPATH}"
    results = []
    db.execute "SELECT project_id, name FROM projects WHERE user_id = ? AND archived = 1", user_id do |row|
      results << {'id' => row[0], 'name' => row[1]}
    end
    return results
  rescue SQLite3::Exception => e
    Core.log "Exception occurred in #{self.class.name}.#{__method__}"
    Core.log e
  ensure
    db.close if db
  end

  def self.update(user_id, project_id, name)
    db = SQLite3::Database.open "#{Core::DBPATH}"
    name = Core.encode name
    db.execute "UPDATE projects SET name = ? WHERE user_id = ? AND project_id = ?", name, user_id, project_id
  rescue SQLite3::Exception => e
    Core.log "Exception occurred in #{self.class.name}.#{__method__}"
    Core.log e
  ensure
    db.close if db
  end

  # Mark a project as archived
  def self.archive(user_id, project_id)
    db = SQLite3::Database.open "#{Core::DBPATH}"
    db.execute "UPDATE projects SET archived = 1 WHERE user_id = ? AND project_id = ?", user_id, project_id
  rescue SQLite3::Exception => e
    Core.log "Exception occurred in #{self.class.name}.#{__method__}"
    Core.log e
  ensure
    db.close if db
  end

  # Unmark a project as archived
  def self.unarchive(user_id, project_id)
    db = SQLite3::Database.open "#{Core::DBPATH}"
    db.execute "UPDATE tags SET archived = 0 WHERE user_id = ? AND project_id = ?", user_id, project_id
  rescue SQLite3::Exception => e
    Core.log "Exception occurred in #{self.class.name}.#{__method__}"
    Core.log e
  ensure
    db.close if db
  end

end
