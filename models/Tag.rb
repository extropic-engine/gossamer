class Tag
  include DataMapper::Resource

  property :id, Serial
  property :name, String, :required => true
  belongs_to :user
  #belongs to many events
  #belongs to many tag groups
end