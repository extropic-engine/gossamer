require '../core.rb'

class Events
  @@month_names = {
    '01' => 'January',
    '02' => 'February',
    '03' => 'March',
    '04' => 'April',
    '05' => 'May',
    '06' => 'June',
    '07' => 'July',
    '08' => 'August',
    '09' => 'September',
    '10' => 'October',
    '11' => 'November',
    '12' => 'December'
  }

  # Adds a new event data record.
  def self.log(user_id, project, start, duration, tags)
    db = SQLite3::Database.open "#{Core::DBPATH}"
    db.execute "INSERT INTO events (user_id, project_id, start, duration) VALUES(?, ?, strftime('%s', ?), ?)", user_id, project, start, duration
    event_id = db.get_first_row("SELECT last_insert_rowid()").first
    return false unless event_id
    tags.each do |tag_id|
      db.execute "INSERT INTO event_tags (event_id, tag_id) VALUES (?, ?)", event_id, tag_id
    end
    return true
  rescue SQLite3::Exception => e
    Core.log "Exception occurred in #{self.class.name}.#{__method__}"
    Core.log e
  ensure
    db.close if db
  end

  # Gets event rows directly for viewing in a table-like format.
  def self.get_event_rows(user_id, start, count, tags=nil)
    db = SQLite3::Database.open "#{Core::DBPATH}"
    results = []
    db.execute "SELECT
          event_id,
          p.name,
          #{Core.pref('timezone') == 'UTC' ? 'datetime' : 'julianday'}(start, 'unixepoch'),
          duration
          #{tags ? ', GROUP_CONCAT(t.name)' : ''}
        FROM events e
        #{tags ? 'LEFT JOIN event_tags USING (event_id) LEFT JOIN tags t USING (tag_id)' : ''}
        JOIN projects p USING (project_id)
        WHERE e.user_id = ?
        GROUP BY event_id
        ORDER BY event_id DESC
        LIMIT #{count} OFFSET #{start}", user_id do |row|
      results << {'event_id' => row[0], 'project' => row[1], 'start' => Core.pref('timezone') == 'UTC' ? row[2] : Float(row[2]).round_to(2), 'duration' => row[3].to_f / 3600.00 , 'tags' => row[4] ? row[4].split(',') : nil}
    end
    return results
  rescue SQLite3::Exception => e
    Core.log "Exception occurred in #{self.class.name}.#{__method__}"
    Core.log e
  ensure
    db.close if db
  end

  # Gets all event data compiled together for the stats view.
  def self.generate_exegesis(user_id)
    db = SQLite3::Database.open "#{Core::DBPATH}"

    # The year structure is a map of year numbers which point to a map of month names which
    # point to arrays. Each element in the array is a map containing the project name and
    # the number of hours spent in that month.
    years = {}
    result = db.execute "SELECT
        strftime('%Y', start, 'unixepoch') AS year,
        strftime('%m', start, 'unixepoch') AS month,
        p.name AS project,
        SUM(duration) AS hours
        FROM events e
        JOIN projects p ON e.project_id = p.project_id
        WHERE e.user_id = ?
        GROUP BY e.project_id, year, month
        HAVING hours >= 3600
        ORDER BY year, month, hours DESC", user_id do |row|
      row[1] = @@month_names[row[1]]
      years[row[0]] = {} if years[row[0]].nil?
      if years[row[0]][row[1]].nil? then
        years[row[0]][row[1]] = []
        years[row[0]][row[1]] << {'project' => 'total', 'hours' => 0}
      end
      years[row[0]][row[1]] << {'project' => row[2], 'hours' => (row[3].to_i / 3600)}
      years[row[0]][row[1]][0]['hours'] += (row[3].to_i / 3600)
    end

    # The hours structure is a map containing totals for each tag as well as the sum of all events.
    hours = {}
    hours['total'] = db.get_first_row("SELECT SUM(duration) FROM events WHERE user_id = ?", user_id).first.to_i / 3600

    # The percents structure is the percent of the total for a given tag.
    percents = {}
    result = db.execute "SELECT
      t.tag_id, name, SUM(duration)
      FROM event_tags et
      JOIN events e USING (event_id)
      JOIN tags t USING (tag_id)
      WHERE e.user_id = ?
      GROUP BY t.tag_id", user_id do |row|
      name = row[1].downcase.gsub(/[^0-9a-z]/i,'') # make tag names safe to use as indicies
      hours[name] = Integer(row[2]) / 3600
      percents[name] = (hours[name] * 100) / hours['total'];
    end

    return {'years' => years, 'hours' => hours, 'percents' => percents}
  rescue SQLite3::Exception => e
    Core.log "Exception occurred in #{self.class.name}.#{__method__}"
    Core.log e
  ensure
    db.close if db
  end
end
