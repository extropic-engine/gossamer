class Inscription
  include DataMapper::Resource

  property :id, Serial
  property :name, String, :required => true, :unique => true
  property :value, String, :required => true
  belongs_to :user
end