require '../core.rb'

class Inscriptions

  def self.carve(user_id, name, value)
    db = SQLite3::Database.open "#{Core::DBPATH}"
    iid = db.get_first_row "SELECT inscription_id FROM inscriptions WHERE user_id = ? AND name = ?", user_id, name
    if iid.nil? then
      db.execute "INSERT INTO inscriptions (user_id, name, value) VALUES (?, ?, ?)", user_id, name, values
    else
      db.execute "UPDATE INSCRIPTIONS SET value = ? WHERE inscription_id = ?", value, iid
    end
  rescue SQLite3::Exception => e
    Core.log "Exception occurred in #{self.class.name}.#{__method__}"
    Core.log e
  ensure
    db.close if db
  end

  def self.read(user_id, name)
    db = SQLite3::Database.open "#{Core::DBPATH}"
    value = db.get_first_row "SELECT value FROM inscriptions WHERE user_id = ? AND name = ?", user_id, name
    return nil if value.nil?
    value = value.first.split ','
    return value.first if value.count == 1
    return value
  rescue SQLite3::Exception => e
    Core.log "Exception occurred in #{self.class.name}.#{__method__}"
    Core.log e
  ensure
    db.close if db
  end

  def self.read_all(user_id)
    db = SQLite3::Database.open "#{Core::DBPATH}"
    inscriptions = {}
    db.execute "SELECT name, value FROM inscriptions WHERE user_id = ?", user_id do |row|
      inscriptions[row[0]] = row[1]
    end
    return inscriptions
  rescue SQLite3::Exception => e
    Core.log "Exception occurred in #{self.class.name}.#{__method__}"
    Core.log e
  ensure
    db.close if db
  end

end
