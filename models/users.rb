require '../core.rb'

require 'digest'

class Users

  def self.add_new(username, password)
    db = SQLite3::Database.open "#{Core::DBPATH}"
    salt = Time.now.to_i
    p_hash = Digest::SHA256.hexdigest "#{password}#{salt}"
    db.execute "INSERT INTO users (username, password, salt) VALUES (?, ?, ?)", username, p_hash, salt
  rescue SQLite3::Exception => e
    Core.log "Exception occurred in #{self.class.name}.#{__method__}"
    Core.log e
  ensure
    db.close if db
  end

  def self.change_password(uid, new_password)
    db = SQLite3::Database.open "#{Core::DBPATH}"
    salt = Time.now.to_i
    p_hash = Digest::SHA256.hexdigest "#{new_password}#{salt}"
    db.execute "UPDATE users SET password = ?, salt = ? WHERE user_id = ?", p_hash, salt, uid
  rescue SQLite3::Exception => e
    Core.log "Exception occurred in #{self.class.name}.#{__method__}"
    Core.log e
  ensure
    db.close if db
  end

  def self.auth(username, password)
    db = SQLite3::Database.open "#{Core::DBPATH}"
    salt = db.get_first_row "SELECT salt FROM users WHERE username = ?", username
    if salt.nil? then
      Core.log "Could not retrieve password salt for user #{username}"
      return false
    end
    p_hash = Digest::SHA256.hexdigest "#{password}#{salt[0]}"
    # Have to put p_hash into the query string directly because of a bug in NFSN's versions of sqlite/gems/ruby.
    # It should be safe because input data goes through hexdigest first.
    user = db.get_first_row "SELECT user_id, preferences FROM users WHERE username = ? AND password = \"#{p_hash}\"", username
    return false if user.nil?
    return {'uid' => Integer(user[0]), 'preferences' => user[1]}
  rescue SQLite3::Exception => e
    Core.log "Exception occurred in #{self.class.name}.#{__method__}"
    Core.log e
  ensure
    db.close if db
  end

  def self.get(user_id)
    db = SQLite3::Database.open "#{Core::DBPATH}"
    user = db.get_first_row "SELECT user_id, preferences FROM users WHERE user_id = ?", user_id
    return false if user.nil?
    return {'uid' => Integer(user[0]), 'preferences' => user[1]}
  rescue SQLite3::Exception => e
    Core.log "Exception occurred in #{self.class.name}.#{__method__}"
    Core.log e
  ensure
    db.close if db
  end

end
