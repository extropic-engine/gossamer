class Mantra
  include DataMapper::Resource

  property :id, Serial
  property :content, String
  property :source, String
  property :is_retired, Boolean, :default => false
  belongs_to :user
end