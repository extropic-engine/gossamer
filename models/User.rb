class User
  include DataMapper::Resource

  property :id, Serial
  property :username, String, :required => true, :unique => true
  property :password, String, :required => true, :length => 64
  property :salt, String, :required => true
  has n, :events
  has n, :mantras
  has n, :inscriptions
  has n, :tags
end